module.exports = (robot) ->
    robot.hear /GGR:(.*)/i, (msg) ->
        encode = encodeURIComponent(msg.match[1])
        msg.send "https://www.google.co.jp/search?q=#{encode}&ie=UTF-8"

