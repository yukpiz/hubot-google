## About

This is hubot script that works with google.  

*Requirements:*  

* Hubot 2.7.5  

*Dependent:*  

* None  




## Installing

**Git cloning**  
~~~sh
$ git clone https://yukpiz@bitbucket.org/yukpiz/hubot-google.git {myhubot}/scripts/hubot-google.git
~~~

**Require script**  
~~~coffee
$ vim require.coffee
+ path = require("path")
+ module.exports = (robot) ->
+     robot.load(path.join(__dirname, "/hubot-google.git/scripts/"))
~~~

Reboot hubot...  




## Usage
~~~sh
hubot> ggr:vim help grep
https://www.google.co.jp/search?q=vim%20help%20grep&ie=UTF-8
hubot> ggr:"vim help grep"
https://www.google.co.jp/search?q=%22vim%20help%20grep%22&ie=UTF-8
~~~





Thanks!  